//Sergio Santana, Carroll, cs570
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include "getword.h"
#include "CHK.h"

#define MAXITEM 100
//these constants are meant to be used with the int ptr flags
//ex. flags[INPUT_RED]
//or ptr[FLAGS][INPUT_RED]
#define INPUT_RED 0 //set if found '<' meaning to redirect input
#define OUTPUT_RED 1 //set if found '>' meaning to redirect output
#define INPUT_ENV 2 //set if found $ in input redirection pointer
#define OUTPUT_ENV 3 //set if found $ in output redirection pointer
#define INPUT_TILDE 4 //set if found a tilde in input redirection pointer
#define OUTPUT_TILDE 5 //set if found a tilde in output redirection pointer
#define PIPE 6 //set if found '|' meaning to redirect output of command before pipe to the input of the command after the pipe
#define BACKGR 7 //set if found a '&' as the last input
#define HERE_IS 8 // set if found '<<' meaning to take lines from stdin write to a temp file until find eof marker specified after. Then redirect input to this temp file/
#define TILDE 9 //set if found a leading '~', meaning im gonna have to replace the user name path in the name after the ~
#define ENV_VAR 10 //set if found '$', meaning I have to replace the environment variable
#define ERROR 11 //set if I enconter error of any kind.

//thse constants are meant to be used with  int ptr 
//ex. ptr[PIPE_OFFSETS]=1;
#define FLAGS 0 //points to the array of flags
#define ERRORS 1 //points to the array of errors
#define PIPE_OFFSETS 2 //points to tje array of pipe_offsets. The cmd in argv where I have to pipe the output
#define ENV_OFFSETS 3 //points to array of offsets in argc where there is a $, meaning we have to replace it with the environment variable if it ecists
#define TILDE_OFFSETS 4 //points to array of offsets in argv where we must replace it with ~xxx.. if it exists 
#define NUM_PIPES 5 //just has number of pipes, Note that the max limit is 10 pipes
#define	NUM_ENV 6 //number of $xxx in argv
#define NUM_TILDES 7 //number of ~xxx.. in argv, Note that if there is a lone tilde or a tilde followed by / then I in Parse I replace it with HOME directory, whatever that may be


//if encounter '<' or '>' must handle input redirection and output redirection respectively. Note: output error if tries to write to file that already exists.
//Similarly, output error if tries to read from file that doesnt exit. 
void redirect(char *input_ptr,int whato);

//replace all the found $ with the specified path returned by getenv()
int replaceEnv(char *argv[], int *num_env, int *env_offset);

int replaceEnvPtr(char *ptr);
int replaceTildePtr(char *ptr);
//used to print the contents of a string array, only used for debuggin
void printArg(char *argv[],int numwords);

//replace the tildes with the user name paths found in the passwd file
int replaceTilde(char *argv[], int *num_tildes, int *tilde_offset);

//handles << redirection, takes a line from stdin and writes to a temp file until ecounter the word following the <<
int hereIs(char *ptr,char *temp_file);

//return 1 if given ptr points to a metachar not preceded by a '\'
int isMeta(char *ptr, int size);

//if encounter '|', skip to different area of code that will handle piping by forking a granchild and more if necessary
void doPipe(char *argv[], int *int_ptrs[], char *input_ptr, char *output_ptr);

//uses free() to deallocate each buffer pointed by each argv[i] that is utilized. Used malloc in parse()
//what I actually pass into size is not simply the amount of malloced pointers in argv, because if there were
//pipes present, then argv is also gonna contain NULL pointers. Hence must pass in numwords+num_pipes. 
void freeString(char *argv[], int size);

//handler that catches SIGNUM but doesnt do anything
void myhandler(int signum);

//gets the "tail" of the given directory, so if "/dev/null/prog" was given. will return the offset of 
//of that pointer where 'p' lies.
int getTail(char *ptr, int size);

int parse(char *argv[], int *int_ptrs[], char *input_ptr, char *output_ptr);
/*
parse() makes use of getword() to analyze the input line word by word until it encounters a ';', newline or EOF. It looks for metachars (<,>,|,&) and sets the 5 flags
accordingly in the int array flags[]:

Also pass in another int ptr "errors" in order to set the correct type of syntax error so that calling main can output necessary message. Goes as follows:

errors[0]=Encountered a '<' or '>' but the name to redirect input/output was missing. 
errors[1]= Encountered '<' more than once or '>' more than once. Ex. "cmd 1 < input1 < input 1". This is ambigous input since I dont know what input to read from.
errors[2]= Encountered more than one pipe '|'. Instructions say to only be able to handle one pipe.
erros[3]= Encountered a pipe '|' with no command after it. An invalid null command.

If I encounter the metachar '<' or '>' then must also save the next word which I assume will be the pathname to where I will redirect input or output. This is the purpose 
of parameters input_ptr and output_ptr. Parse does not care whether it is an actual pathname, that will be handled by the calling code. 
If I encounter the metachar '&' as the LAST WORD in the input line then set flag[3] and calling main will handle it. 

There is an important exception to the above rules. getword() might return a metachar, however, this metachar might've been preceded by a backslash, which means I should
not treat as an actual metachar, but as just another word in the input. 
Example: "cmd arg1 \&" In this case '&' is not a metachar, its just another argument to the command. I have set up a global variable called meta_char in getword.c that 
will unset if this event happens denoting that I should treat the current metachar as an argument instead.


If I encounter a word that is not a metachar and not preceded by a metachar, then parse() assumes this to be the name of an executable command, and places this in the parameter argv[0] using strcpy().
All subsequent words that follow the above rule are assumed to be arguments to the executable, and will also be placed in argv[1,2,3...] using strcpy().

If encounter a pipe= "|" it means to send the output of the first command to the input of the 2nd command. Parse() is only concerned with null terminating the argument array argv
with a null='\0', and also it saves the index in the argv array where the 2nd command is located and subsequent arguments to that 2nd command start.

The parameters modified by Parse() will look like this assuming generic input "cmd1 $arg1 < ~file1 | cmd2 ~arg2 > $file2 | cmd3 & "

I actually place all the necessary integers and integer arrays (num_pipes,pipe_offsets,flags,errors, env_offsets) into an array of int pointers so that parse
does not have a billion arguments I only have to pass argv,input_ptr,output_ptr and int_ptrs.


Parse() does the following for input " cmd1 $arg1 < ~file1 | cmd2 ~arg2 > $file2 | cmd3 & "

argv[] :
[0]="cmd1"
[1]='$arg1'  (in calling main arg1 will be replaced by its environment variable path if it exists)
[2]=\0
[3]=cmd2
[4]="~arg2"
[5]=\0
[6]= $file2
[7]=\0
[8]=cmd3

input_ptr=~file1
output_ptr=$file2


flags[]:
[INPUT_RED]=1 (input redirection)
[OUTPUT_RED]=1 (output redirection)
[PIPE]=1 (At least one Pipe present)
[BACKGR]=1 (ampersand, background process)
[ERROR]=0 (syntax error, above example does not have syntax error)
[INPUT_TILDE]=1 (~file1)
[OUTPUT_TILDE]=0 
[BACKGR]=1 (last &)
[INPUT_ENV]=1 ($file2)
[OUTPUT_ENV]= 1 ($file will replace with environment variable path)
[TILDE]=1 (only applies to 'words' argv so ~arg2)
[HERE_IS]=0 (no << present)


ptrs[]:
[FLAGS]= points to array of all the flags mentioned above
[ERRORS]= all elements will be zero bc theres no errors of any kind
[PIPE_OFFSETS]= [0]=0, [1]=3, [2]=6
[ENV_OFFSETS]= [0]=1
[TILDE_OFFSETS]=[0]=4
[NUM_PIPES]=2
[NUM_ENV]=1
[NUM_TILDES]=1  


Null terminate the argv array and return the number of words.
*/
