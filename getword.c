//Sergio Santana, Carroll, CS570, due 11/28/18 
#include "getword.h"
#include <stdlib.h>

/*getword() takes input from stdin and counts the number of chars (in charCount) until it encounters a delimiter (;,newline,EOF, space...). Then it returns charCount. Does this for every
*word on the stream until it ecounters EOF which will then return -255. There are special cases such as: if first char in word is "~" then replace with $HOME directory. Also, if first
*char is "$" then return the next charCount as a negative number.  
*/
int global_fake_meta;
int getword(char *w){
	int iochar;
	int charCount=0;
	
	//if first char is '$', will turn negative before returning
	int negPro=0;
	
	//just need for one special case, namely "<<"
	int nextChar=0;
	
	/*will be set if encounter '\', means to take in whatever
	*subsequent metachar except for newline, which I should treat as space
	*/
	int skip_meta=0;
	
	//break from while loop if encounter a ;, \n or EOF
	int break_loop=0;
	
	//if encounter a tilde at the beginning but was preceded by a slash,
	//need this var
	int fake_tilde_mode=0;
	
	int real_tilde_mode=0;
	
	//this var is extremely important. If the input is '\<" or '\>' or '\$'...
	//the char returned will be exactly as if the slash was not there, hence
	//need this var in order to detect a "fake" meta char, in other words, a 
	//lone metachar that was preceded by a char.
	global_fake_meta=0;
	
	
	/*loop to get input from stdin
	*very important that iochar=getchar() is
	*second condition, or else would need to ungetc
	*for every loop even if break_loop is set
	*/

	while( !break_loop && (iochar=getchar())!=EOF){
		
		if(skip_meta && iochar=='~' && charCount==0 && !negPro){
			fake_tilde_mode=1;
			global_fake_meta=1;
			}
		else if(!skip_meta && iochar=='~' && charCount==0 && !negPro){
			real_tilde_mode=1;
			global_fake_meta=0;
			}
		
		/*buffer size is 255, but need last one for null terminator,
		*must ungetc because it is wasted on iochar=getchar()
		*/
		
		if (charCount==254){
			ungetc(iochar,stdin);
			w++; 
			break_loop=1;
			}
		
		/*if char is  space, skip if charCount ==0 (leading space)
		*and if charCount not 0 then break out and return charCounts,
		*if skip_meta set and iochar is newline, treat as space (from instructions)
		*/
		else if ( (iochar==' '&& !skip_meta) || (iochar=='\n' && skip_meta) )
			if(charCount==0) ;
			else break_loop=1;
		
		/*the reason why I don't group these metacharacters with the 
		*other metacharacters from the below "else if" 
		*is because they have different behavior than "newlines, ; and spaces"
		*namely to return 1 (or 2 when <<) and not 0
		*/
		else if((iochar=='<'|| iochar=='>'|| iochar=='|' || iochar=='&')
		&& !skip_meta){
			
			/*if have collected chars, must first return
			*charCount before handling meta char. Since 
			*Ive waisted iochar must ungetc
			*/
			if(charCount!=0) ungetc(iochar,stdin);
			
			/*return 2 if metachars is "<<"
			*else return 1. Still have to ungetc because
			*Ive waisted the iochar in checking to see
			*if the next char was another "<"
			*/ 
			else{
				if( (nextChar=getchar())==iochar && iochar=='<') {
					charCount=2;
					*w=iochar;
					w[1]=nextChar;
					w=w+2;
					}
				else{
					ungetc(nextChar,stdin);
					charCount=1;
					*w=iochar;
					w++;
					}
				}	
			break_loop=1;
		}	
		
		/*if iochar is newline or colon, then must use ungetc
		*to place it back on stream, not necessary if have collected
		*no chars. Must break out of loop either way
		*/
		else if (iochar=='\n'||iochar==';'&& !skip_meta){
			if(charCount!=0 || (negPro==1 && charCount==0)) ungetc(iochar,stdin);
			break_loop=1;	
			}
			

		/*if char is $ and first char, then will begin negative
		*procedure, (to flip sign of charCount in the end).
		*I provide a seemingly strange "negpro==0" condition
		*bc if I encounter "$$..." it wouldn't count the second '$'
		*like it should since charCount would be 0  
		*/
		else if (iochar=='$' && charCount==0 && negPro==0 && !skip_meta) negPro=1;
		
		//if iochar is a \ then will take in anything (even metacharacters)
		else if (iochar=='\\' && !skip_meta ){
			skip_meta=1;
			}	
		
		/*if iochar is an actual permissible char and not a metacharacter
		*where it wasnt preceded by a "\" then write w byte and increment 
		*charCount and w.
		*/
		else{
			if(fake_tilde_mode || real_tilde_mode) ;
			else if (skip_meta) global_fake_meta=1;
			else global_fake_meta=0;
			skip_meta=0;
			
			*w=iochar;
			charCount++;
			w++;
			}
	}
	/*if reached EOF without any chars. 
	*I include another weird !negpro condition
	*for the case where an EOF follows a first char
	*"$". This is doing negative procedure
	*and should not return -255.
	*/
	if(iochar==EOF && charCount==0 && !negPro ) charCount=-255;
	
	//flip sign if doing "$" negative procedure
	if(negPro) charCount=charCount*-1;
	
	
	//append null terminator to print correctly in caller code
	*w='\0';
	
	//ONLY ONE RETURN STATEMENT, makes it more readable I like to think
	return charCount;

}
