//Sergio Santana, Carroll, CS570
 
#include "p2.h"

extern int global_fake_meta;

/*
*Main creates a command line interpreter that prompts the user for a command plus arguments and forks a child to execute this command.
*It calls parse to analyze each input line using getword(). Stops prompting when encounters EOF and no input lines left. Must use
*my created freeString to deallocte the malloced storaged used in Parse using free(ptr).    
*WHAT EACH FLAGS[x] AND ERRORS[X] REPRESENTS IS EXPLAINED PROFUSELY IN p2.h
[B*/
//simple handler that does nothing, just catches the signal
void myhandler(int signum){
	//printf("Received SIGTERM (%d), and the special handler is running...\n", signum);	
	;
}
 
main(){
	//I choose not to allocate storage for each char ptr before calling parse() in argv[]
	//because I have no idea how many char ptrs Im going to use.
	//Of course I can allocate based on MAXITEM and STORAGE.
	//But that would mean allocating (100 * 255= 25500) bytes in total on the stack
	//for each input line, which I think is wasteful.
	//hence I choose to use dynamic allocation inside of Parse.
	char *argv[MAXITEM];
	
	char input_ptr[STORAGE];
	char output_ptr[STORAGE];
	int  *int_ptrs[8];
	int flags[12]={0};
	int errors[4]={0};
	int pipe_offset[10];
	int env_offset[10];
	int tilde_offsets[10];
	int num_env;
	int num_pipes;
	int num_tildes;
	int kidpid=1;
	int numwords=0;
	char prompt[255]=":570: ";
	srand(time(0));

	//place all int pointers into an array of pointers
	//so that there are not so many arguments into parse() and doPipe()
	
	int_ptrs[FLAGS]=flags;
	int_ptrs[ERRORS]=errors;
	int_ptrs[PIPE_OFFSETS]=pipe_offset;
	int_ptrs[ENV_OFFSETS]=env_offset;
	int_ptrs[TILDE_OFFSETS]=tilde_offsets;
	int_ptrs[NUM_PIPES]=&num_pipes;
	int_ptrs[NUM_ENV]=&num_env;
	int_ptrs[NUM_TILDES]=&num_tildes;

	(void) setpgid(0,0); 
	(void) signal(SIGTERM,myhandler);

	for(;;){
		//set all flags and errors to 0 before proceding
		//this is necessary, 
		memset(flags,0,sizeof(int)*12);
		memset(errors,0,sizeof(int)*4);
		
		*int_ptrs[NUM_ENV]=0;
		*int_ptrs[NUM_PIPES]=0;
		*int_ptrs[NUM_TILDES]=0;

		(void) fprintf(stdout,"%s",prompt);
		numwords=parse(argv,int_ptrs,input_ptr,output_ptr);
		//printArg(argv,numwords+num_pipes);
		if(numwords==-255){
			freeString(argv, numwords+num_pipes);
			break;
			}
		//output any syntax error message based on what parse analyzed
		if(flags[ERROR]){
			if(errors[0])
				(void) fprintf(stderr,"Missing name for redirect.");
			else if(errors[1])
				(void)fprintf(stderr,"Ambiguous input redirect.");
			else if(errors[2])
				(void) fprintf(stderr,"Can only handle 10 pipes maximum.");
			else if(errors[3])
				(void) fprintf(stderr,"Invalid null command.");
			(void) fprintf(stderr,"\n");
			
			freeString(argv, numwords+num_pipes);
			continue;
			}
		if(numwords==0) {
			//must free the malloced storage in argv[]
			freeString(argv, numwords+num_pipes);
			continue;
			}

		if(flags[ENV_VAR]){
			
			if(replaceEnv(argv,&num_env,env_offset)==-1){
				fprintf(stderr,"%s: Undefined variable.\n",argv[num_env]);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			//do nothing, replace env already did the necessary work	
			}
		if(flags[INPUT_ENV]){
			
			if(replaceEnvPtr(input_ptr)==-1){
				fprintf(stderr,"%s: Undefined variable.\n",input_ptr);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			//do nothing, replace env already did the necessary work	
			}
		if(flags[OUTPUT_ENV]){
			if(replaceEnvPtr(output_ptr)==-1){
				fprintf(stderr,"%s: Undefined variable.\n",output_ptr);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			//do nothing, replace env already did the necessary work	
				
			}
		if(flags[TILDE]){
			int offs;
			if( (replaceTilde(argv,&num_tildes,tilde_offsets))==-1){
				fprintf(stderr,"%s: Unknown user.\n",argv[num_tildes]+1);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			}
		if(flags[INPUT_TILDE]){
			if( (replaceTildePtr(input_ptr))==-1){
				fprintf(stderr,"%s: Unknown user.\n",input_ptr+1);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			}
		if(flags[OUTPUT_TILDE]){
			if( (replaceTildePtr(output_ptr))==-1){
				fprintf(stderr,"%s: Unknown user.\n",output_ptr+1);
				freeString(argv,numwords+num_pipes);
				continue;
				}
			}
			

		if(strcmp(argv[0],"cd")==0) {
			if(numwords > 2) fprintf(stderr,"cd has too many arguments\n");
			else if(numwords==1){ 	
				if(chdir(getenv("HOME"))==-1) perror(getenv("HOME"));
				
				else{
					char buffer[255];
					int offset;
					getcwd(buffer,255);
					if(strlen(buffer)==1 && *buffer=='/'){
						prompt[0]='/';
						strcpy(prompt+1,":570: ");
						
						}
					else{
						offset=getTail(buffer,strlen(buffer));
						(void) strcpy(prompt,buffer+offset);
						(void) strcpy(prompt+strlen(prompt),":570: ");
						}
					}
				}
				
			else if(numwords==2){
				
				if(chdir(argv[1])==-1)
					perror(argv[1]);
										
					
				else {	
					//a place to store the current directory, need to extract "Tail"
					char buffer[255];
					int offset;
					(void) getcwd(buffer,255);
					if(strlen(buffer)==1 && *buffer=='/'){
						prompt[0]='/';
						strcpy(prompt+1,":570: ");
						}
					else {
						offset=getTail(buffer,strlen(buffer));
						(void) strcpy(prompt,buffer+offset);
						(void) strcpy(prompt+strlen(prompt),":570: ");
						}
					}
				}
			freeString(argv, numwords+num_pipes);
			continue;
			}
		if(strcmp(argv[0],"environ")==0){
			
			if(numwords > 3 || numwords==1) fprintf(stderr,"environ accepts only 1 or 2 arguments.\n");
			
			else if (numwords==2){
				char *buffer;
				if( (buffer=getenv(argv[1])) == NULL)
					;
				else
					fprintf(stdout,"%s\n",buffer);
				}
			else if (numwords==3){
				char *buffer;
				
				if(setenv(argv[1], argv[2],1)==-1)
					perror("");
				}			
				
			freeString(argv,numwords+num_pipes);
			continue;
			}
		if(flags[HERE_IS]){
			int randN;
			char buffer[255];
			//ridiculous name for a file, very unlikely this name will be present in the current dirrectory
			char *string="KABLOOMIE_SHABLOOMIE_TEMP";
			//for added security, append a random number
			randN=rand();
			sprintf(buffer, "%d", randN);
			
			strcpy(buffer+strlen(buffer),string);
			hereIs(input_ptr,buffer);
			
			//now will redirect output to this temporary file
			flags[INPUT_RED]=1;

			}	
		(void) fflush(stdout);
		(void) fflush(stderr);
		
		if( (kidpid=(int) fork())==-1){
			//cannot fork, still in parent :(
			perror("Cannot fork");
			if(flags[HERE_IS])	
				//must delete temp file if used one
				remove(input_ptr);
			
			freeString(argv, numwords+num_pipes);
			continue;
			}
		
		else if ( kidpid == 0){
			//now in child
			
			/*use this var to check if I need to redirect input to
			*dev/null if doing a background process . 
			*If I already redirected input to some file
			*then theres no need to do this. 
			*/
			

			/*use this var to break out if already jumped to doPipe
			*doPipe already handles input/output redirection if 
			*applicable.
			*/
			
			if(flags[PIPE]) 
				doPipe(argv,int_ptrs,input_ptr,output_ptr);
				
			if(flags[INPUT_RED] && !flags[PIPE]) 
				redirect(input_ptr,STDIN_FILENO);
			
			if(flags[OUTPUT_RED] && !flags[PIPE])
				redirect(output_ptr,STDOUT_FILENO);
			
			if(flags[BACKGR] && !flags[INPUT_RED])
				redirect("/dev/null", STDIN_FILENO);							
			
			if(!flags[PIPE]){
				if(-1==execvp(argv[0],argv)){
					perror(argv[0]);
					exit(3);
					}
				}
			}	
		//wait until child dies if not doing bckground process, want to make sure its correct child
		while(!flags[BACKGR]){
			int temp_pid;
 			CHK(temp_pid=wait(NULL));
			if(temp_pid==kidpid)
				break;
			}
		//if background process flag is set, then DONT WAIT, just print below
		if(flags[BACKGR]) fprintf(stdout,"%s [%d]\n",argv[0],kidpid);

		//very last thing I should do,because I use malloc
		//in parse, I should free up space when I dont need the strings anymore
		if(flags[HERE_IS])	
			//must delete temp file if used one
			remove(input_ptr);
		freeString(argv, numwords+num_pipes);
		
	
	}
	killpg(getpgrp(),SIGTERM);
	(void) fprintf(stdout,"p2 terminated.\n");	
  	exit(2);
}

//used for debuggin, just checks contents of argv array
void printArg(char *argv[], int numwords){

	int i=0;
	
	while(i<numwords){
		printf("Argv[%d]:%s\n",i,argv[i]);
		i++;
		}	

}

//handles the << redirection, creates a temp
//file with a ridiculous name that will be
//deleted later when the process exits
int hereIs(char *ptr, char *temp_file){
	char *line;
	size_t size;
	FILE *file;
	file=fopen(temp_file,"ab+");

	while(getline(&line, &size, stdin)!=-1){
		strtok(line,"\n");
		
		//if found the eof keyword, dont write, just break 
		if(strcmp(ptr,line)==0){
			break;
			}
		else{
			//write line to temp file
			fprintf(file,"%s\n",line);			
			}
			
			
		}
	fclose(file);
	//copy the tempfile name to input_ptr so that it works correctly
	//when the forked process calls redirect(..) (dont have to write extra code)
	strcpy(ptr,temp_file);	
	return 1;		


}
//replace $xxxx with the correct environment variable 
//if it exists, if not than output error
int replaceEnv(char *argv[], int *num_env, int *env_offset){
	int i=0;
	int size_env=0;
	while(i<*num_env){
		
		char *buffer;
		//if var does not exist, return -1 to signal that
		//it failed and set the offset in num_env
		//so I can print a message "VAR: does not exits..."
		if( (buffer=getenv(argv[env_offset[i]]))==NULL){
			*num_env=env_offset[i];
			return -1;
			}
		free(argv[env_offset[i]]);
		size_env=strlen(buffer)+1;
		argv[env_offset[i]]=malloc(size_env);
		strcpy(argv[env_offset[i]],buffer);	
		i++;		
		}
	return 1;
}

//replace $xxxx with the correct environment variable 
//if it exists, if not than output error
int replaceEnvPtr(char *ptr){
	int size_env=0;
	
	char *buffer;
	//if var does not exist, return -1 to signal that
	//it failed and set the offset in num_env
	//so I can print a message "VAR: does not exits..."
	if( (buffer=getenv(ptr))==NULL){
		return -1;
		}
	strcpy(ptr,buffer);	
	
	return 1;
}



//must replace the username given by ~ found in the /etc/passwd file. (if  it exists)
//returns -1 if failed, 0 if successful
//if it failed, then places the offset of the 1st failed user name look up
//in num_tildes so that I can print the name of the failed user name in the calling main
int replaceTilde(char *argv[], int *num_tildes, int *tilde_offset){
	FILE *file;
	int success=0;
	int i=0;
	

	while (i<*num_tildes){
		char *line;
		char *tok_line;
		char *tok_line2;
		size_t length;
		ssize_t read;
		int size;
		char save_argv[255];
		length=0;
		success=-1;
		if( (file=fopen("/etc/passwd","r"))==NULL){
			return -1;
			}
		strcpy(save_argv,argv[tilde_offset[i]]);
					
		tok_line=strtok(argv[tilde_offset[i]]+1,"/");
		size=strlen(tok_line);
 
		while( getline(&line, &length, file) != -1){
			tok_line2=strtok(line,":");
			if(strcmp(tok_line,tok_line2)==0){
				char buffer[255];
				success=1;
				strtok(NULL,":");
				strtok(NULL,":");
				strtok(NULL,":");
				strtok(NULL,":");
				tok_line2=strtok(NULL,":");
				strcpy(buffer,tok_line2);
				strcpy(buffer+strlen(tok_line2),save_argv+size+1);				
				free(argv[tilde_offset[i]]);
				argv[tilde_offset[i]]=malloc(strlen(buffer)+1);
				(void) strcpy(argv[tilde_offset[i]],buffer);
				}
			}	
		if(success==-1){
			
			 *num_tildes= tilde_offset[i];
			return -1;
			}
		i++;		
		}
	fclose(file);
		
	return success;
	
}


int replaceTildePtr(char *ptr){
	FILE *file;
	int success=0;
	int i=0;
	

	char *line;
	char *tok_line;
	char *tok_line2;
	size_t length;
	ssize_t read;
	int size;
	char save_ptr[255];
		
	length=0;
	success=-1;
	if( (file=fopen("/etc/passwd","r"))==NULL){
		return -1;
		}
	strcpy(save_ptr,ptr);
					
	tok_line=strtok(ptr+1,"/");
	size=strlen(tok_line);
 
	while( getline(&line, &length, file) != -1){
		tok_line2=strtok(line,":");
		if(strcmp(tok_line,tok_line2)==0){
			char buffer[255];
			success=1;
			strtok(NULL,":");
			strtok(NULL,":");
			strtok(NULL,":");
			strtok(NULL,":");
			tok_line2=strtok(NULL,":");
			strcpy(buffer,tok_line2);
			strcpy(buffer+strlen(tok_line2),save_ptr+size+1);				
			(void) strcpy(ptr,buffer);
			}
	}	
	if(success==-1) return -1;
			
				
		
	fclose(file);
		
	return success;
	
}





int getTail(char *ptr, int size){
	int offset=-1;
	while(size>=0){
		if(ptr[size]=='/'){
			offset=size+1;
			break;
			}
		size--;	
		}
		
	return offset;

}
//free up all the space allocated by malloc in parse()
void freeString(char *argv[], int to_free){
	int m=0;
	
	while(m < to_free){
		if(m==MAXITEM) break;
		if(argv[m]==NULL) ;
		else free(argv[m]);
		m++;
		}
	
}

//handles the piping, using one child to execute 2nd process and grandchild
//to execute 1st process. Grandchild finishes and writes to the pipe while
//child reads from it
void doPipe(char *argv[], int *ptrs[], char *input_ptr, char *output_ptr){
	int num_pipes=*ptrs[NUM_PIPES];
	int fildes[20];
	int gpid;
	int p1_read=0;
	int p1_write=1;
	int p2_read=0;
	int p2_write=1;
	if(ptrs[FLAGS][OUTPUT_RED]) redirect(output_ptr,STDOUT_FILENO);
	CHK(pipe(fildes));
	fflush(stdout);
	fflush(stderr);
	CHK(gpid=(int) fork());
	
	if(gpid==0){
TOP:		
		num_pipes--;
		if(num_pipes==0){
			if(ptrs[FLAGS][INPUT_RED]) redirect(input_ptr,STDIN_FILENO);
			CHK(dup2(fildes[p1_write],STDOUT_FILENO));
			CHK(close(fildes[p1_read]));
			CHK(close(fildes[p1_write]));
			if(-1==execvp(argv[ptrs[PIPE_OFFSETS][num_pipes]],argv+ptrs[PIPE_OFFSETS][num_pipes])){
				perror(argv[ptrs[PIPE_OFFSETS][num_pipes]]);
				exit(3);
				}

			}
		else{
			p2_read=p2_read+2;
			p2_write=p2_write+2;
			CHK(pipe(fildes+p2_read));
			fflush(stdout);
			fflush(stderr);

			CHK(gpid= (int) fork());
			if(gpid==0){
				CHK(close(fildes[p1_read]));
				CHK(close(fildes[p1_write]));
				p1_read=p1_read+2;
				p1_write=p1_write+2;
				goto TOP;
				}
			CHK(dup2(fildes[p1_write],1));
			CHK(dup2(fildes[p2_read],0));
			
			CHK(close(fildes[p1_read]));
			CHK(close(fildes[p1_write]));
			CHK(close(fildes[p2_read]));
			CHK(close(fildes[p2_write]));
			if(-1==execvp(argv[ptrs[PIPE_OFFSETS][num_pipes]],argv+ptrs[PIPE_OFFSETS][num_pipes])){
				perror(argv[ptrs[PIPE_OFFSETS][num_pipes]]);
				exit(3);
				}

			}
			
		}

	CHK(dup2(fildes[0],STDIN_FILENO));
	CHK(close(fildes[0]));
	CHK(close(fildes[1]));
	
	if(-1==execvp(argv[ptrs[PIPE_OFFSETS][num_pipes]],argv+ptrs[PIPE_OFFSETS][num_pipes])){
		perror(argv[ptrs[PIPE_OFFSETS][num_pipes]]);
		exit(3);
		}


}
//simply return 1 if given pointer is a true metachar, else return 0
int isMeta(char *ptr, int size){

	if( size ==1 && (*ptr=='<' || *ptr=='>' || *ptr=='|' || *ptr=='&') && !global_fake_meta)
		return 1;
	else if (size ==2 && (*ptr=='<' && *(ptr+1)=='<') && !global_fake_meta)
		return 1;
	return 0;
}

//handles redirection of stdin or stdout without a pipe
void redirect(char *redirect_ptr,int whato){
	int redirect_fd;
	//check if input redirect file exists
	if(whato==STDIN_FILENO) 
		if(( redirect_fd=open(redirect_ptr,O_RDONLY))==-1){
				perror(redirect_ptr);
				exit(3);
				}
	if(whato==STDOUT_FILENO) {
		//cannot redirect output to a file that already exists
		if(access(redirect_ptr,F_OK)==0){
			(void) fprintf(stderr,"%s: File exists.\n",redirect_ptr);
			exit(6);	
			}
		//create the file to redirect, already made sure above it doesnt exist
		CHK(redirect_fd=open(redirect_ptr,O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR));
				
		}
	
	if(dup2(redirect_fd,whato)==-1){
		perror(redirect_ptr);
		exit(2);
		}
		
	CHK(close(redirect_fd));
}

int parse(char *argv[], int *ptrs[], char *input_ptr, char *output_ptr){
	int i=0;
	char w[STORAGE];
	int wordSize;
	int numwords=0;
	int size;
	int last_word_amper;
	int *save_pipe_offset=ptrs[PIPE_OFFSETS];
	int *save_env_offset=ptrs[ENV_OFFSETS];
	int *save_tilde_offset=ptrs[TILDE_OFFSETS];
	
	/*The purpose of the below variable and the following ifs associated with it is annoyingly
	*complicated. If encounter an ampersand on the input line then set this variable.
	*It might or might not be the last word on the line. If its not, we enter the 
	*while loop again and check if we had encountered a "false" ampersand (not the last word).
	* (Keep in mind that this is different from the other "false" ampersand when its preceded 
	*by a '\'. This is already handled when calling trueMeta() )
	*If it enters "if(last_word_amper) ...." then we must place the "&" on the argv array
	*since non-last ampersands are treated as simple arguments.
	*If the ampersand HAD been the last word on the line, the while loop ends 
	*and the "if(last_word_amper)..." sets the required flag in flags[3]
	*/   
	last_word_amper=0;
	
	*ptrs[PIPE_OFFSETS]=0;
	ptrs[PIPE_OFFSETS]++;
	
	
	//exit loop if encounter newline, colon or EOF
	//numwords and i variables can be used interchangeably, but I
	//chose to use i for indexing argv and numwords to keep track of # strings in argv[]
	//have a if(numwords==99) check to make sure we dont pass the limit of 100 words on a line
	//must use last index for NULL terminator, which is why its 99.
	//Note: numwords does not include the NULL pointer, I chose to include the NULL in numwords
	//because when checking if (numwords==99) is had to keep the null in mind
	while( (size=getword(w)) != -255 && size!=0){
		if(last_word_amper){
			if(i==99) break;
			//ampersand wasnt the last on line
			//must malloc 2 bytes to place
			//& on argv[] and null pointer
			last_word_amper=0;
			argv[i]=malloc(2);
			*(argv[i])='&';
			*(argv[i]+1)='\0';
			i++;
			numwords++;
			}
		/*
		*look for each metachar and handle accordingly setting appropriate
		*flag and errors as described in "p2.h".
		*the reason why I dont simply write the ifs as 
		*such: if(*w==<) is because you might have the case 
		*"\<word". So *w equal "<", but its not a metachar
		*because its preceded by a slash. Hence we 
		*have to check size to make sure only one char 
		*was read by getword(w).  trueMeta(w) protects against
		*the special case : "cmd1 /< arg1" . In this case 
		*getword(w) would return 1 when reading "\<",
		* so the (size==1) condition is not 
		* enough to protect against false metachars. This 
		*is the purpose of trueMeta(w) which utilizes the global
		*var fake_meta to check if it was an isolated metachar
		*not preceded by a slash.    
		*/
		if( size==1 && *w=='<' && !global_fake_meta  ){
			//found metachar, make sure next word is also a word and not another metachar
			if(ptrs[FLAGS][INPUT_RED] || (size=getword(w))==-255 || size==0 || isMeta(w,size)  ){
				if (ptrs[FLAGS][INPUT_RED])
					ptrs[ERRORS][1]=1;
				else if(size==-255 || size==0 || isMeta(w,size))
					ptrs[ERRORS][0]=1;

				ptrs[FLAGS][ERROR]=1; 
				if(size==0 || size==-255)
					break;
								
				}
			//no errors, set flag and break
			
			if(size<0)
				ptrs[FLAGS][INPUT_ENV]=1;
			else if( (size==1 && *w=='~' && !global_fake_meta ) || (*w=='~' && *(w+1)=='/' && !global_fake_meta  ) ){
				char buffer[255];
				char *temp=getenv("HOME");
				strcpy(buffer,temp);
				strcpy(buffer+strlen(temp),w+1);
				(void) strcpy(w,buffer);
				}
			else if(*w=='~' && !global_fake_meta )
				ptrs[FLAGS][INPUT_TILDE]=1;
				
			ptrs[FLAGS][INPUT_RED]=1;
			(void) strcpy(input_ptr,w);
			}
		else if(size==1 && *w=='>' && !global_fake_meta ){
			//found metachar, make sure next word is also a word and not another metachar
			//also make sure I havent already seen this metachar (instructions say to handle only one)
			if(ptrs[FLAGS][OUTPUT_RED] || (size=getword(w))==-255 || size==0 || isMeta(w,size)){
				if (ptrs[FLAGS][OUTPUT_RED])
					ptrs[ERRORS][1]=1;
				else if(size==-255 || size==0 || isMeta(w,size))
					ptrs[ERRORS][0]=1;
				
				ptrs[FLAGS][ERROR]=1;
				if(size==0 || size==-255)
					break;

				}
			//no errors, set flag and break
			if(size<0)
				ptrs[FLAGS][OUTPUT_ENV]=1;
			else if( (size==1 && *w=='~' && !global_fake_meta ) || (*w=='~' && *(w+1)=='/' && !global_fake_meta  ) ){
				char buffer[255];
				char *temp=getenv("HOME");
				strcpy(buffer,temp);
				strcpy(buffer+strlen(temp),w+1);
				(void) strcpy(w,buffer);
				}
			else if(*w=='~' && !global_fake_meta )
				ptrs[FLAGS][OUTPUT_TILDE]=1;
			
			ptrs[FLAGS][OUTPUT_RED]=1;
			strcpy(output_ptr,w);
			}
		//if ecounter a lone tilde or a tilde followed by a /, then replace HOME variable
		//so that I dont have to do it in calling main	
		else if( (size==1 && *w=='~' && !global_fake_meta ) || (*w=='~' && *(w+1)=='/' && !global_fake_meta  ) ){
			char buffer[255];
			char *temp=getenv("HOME");
			strcpy(buffer,temp);
			strcpy(buffer+strlen(temp),w+1);
			argv[i]=malloc(strlen(buffer)+1);
			(void) strcpy(argv[i],buffer);
			i++;
			numwords++;
			}
		
		else if(size==1 && *w=='|' && !global_fake_meta ){
			//found metachar, make sure next word is also a word and not another metachar
			//also make sure metachar is no more than 10 (instructions say to handle only 10)
			if(*ptrs[NUM_PIPES]>10 || (size=getword(w))==-255 || size==0 || isMeta(w,size) || numwords==0 ){
				if (*ptrs[NUM_PIPES]>10)
					ptrs[ERRORS][2]=1;
				else if(size==-255 || size==0 || isMeta(w,size) || numwords==0 )
					ptrs[ERRORS][3]=1;
				//set error flag
				ptrs[FLAGS][ERROR]=1; 
				if(size==0 || size==-255)
					break;

				}
			//if no errors, than null terminate argv at i,
			//and then place pipe command on next element
			if(i==99) break;
			ptrs[FLAGS][PIPE]=1;
			argv[i]=NULL;
			i++;
			numwords++;
			if(size<0){
				ptrs[FLAGS][ENV_VAR]=1;
				*ptrs[ENV_OFFSETS]=i;	
				ptrs[ENV_OFFSETS]++;
				*ptrs[NUM_ENV]=*ptrs[NUM_ENV]+1;
				}
			else if( (size==1 && *w=='~' && !global_fake_meta ) || (*w=='~' && *(w+1)=='/' && !global_fake_meta  ) ){
				char buffer[255];
				char *temp=getenv("HOME");
				strcpy(buffer,temp);
				strcpy(buffer+strlen(temp),w+1);
				(void) strcpy(w,buffer);
				}
			else if(*w=='~' && !global_fake_meta ){
				ptrs[FLAGS][TILDE]=1;
				*ptrs[TILDE_OFFSETS]=i;
				ptrs[TILDE_OFFSETS]++;
				*ptrs[NUM_TILDES]=*ptrs[NUM_TILDES]+1;				
				}
			
			
			//now copy command after pipe into argv[i]
			argv[i]=malloc(strlen(w)+1);
			(void) strcpy(argv[i],w);
			
			//keep offset saved where command after pipe begins in argv[]
			*ptrs[PIPE_OFFSETS]=i;
			ptrs[PIPE_OFFSETS]++;
	
			
			i++;
			*ptrs[NUM_PIPES]=*ptrs[NUM_PIPES]+1;			
			}
		//if ecounter the here is identifer, set flag
		else if (size==2 && *w=='<' && *(w+1)=='<' && !global_fake_meta){
			
			if(ptrs[FLAGS][HERE_IS] || (size=getword(w))==-255 || size==0 || isMeta(w,size) ){
				if (ptrs[FLAGS][HERE_IS])
					ptrs[ERRORS][1]=1;
				else if(size==-255 || size==0 || isMeta(w,size))
					ptrs[ERRORS][0]=1;
				//found an error

				ptrs[FLAGS][ERROR]=1; 
				if(size==0 || size==-255)
					break;
				}
			//no errors, set flag and break
			ptrs[FLAGS][HERE_IS]=1;
			(void) strcpy(input_ptr,w);

			}
		else if(size==1 && *w=='&' && !global_fake_meta)
			last_word_amper=1;
		
		//if encounter $....mus get environment variable
		
			
			
		/*if word is not a metachar, copy into argv[i]
		*using dynamic allocation. The reason why
		*I use dynamic allocation is because when declaring
		*a pointer to an array of char pointers (argv),
		*I also need to declare a buffer for each 
		*char pointer that I plan to use. And since I 
		*have no idea how many char pointers Im gonna 
		*need, I feel its best to allocate as soon
		*as I need it. And since we need these for main()
		*using malloc is necessary.
		*/  
		else{

			if(i==99) break;
			argv[i]=malloc(strlen(w)+1);
			(void) strcpy(argv[i],w);
			
			//if encounter $....mus get environment variable
			if(size<0){
				ptrs[FLAGS][ENV_VAR]=1;
				*ptrs[ENV_OFFSETS]=i;	
				ptrs[ENV_OFFSETS]++;
				*ptrs[NUM_ENV]=*ptrs[NUM_ENV]+1;
				}
			//if ecounter a tillde thats not alone 
			//and not followed by a /, then must set
			//flag to replace user name path	
			else if(*w=='~' && !global_fake_meta ){
				ptrs[FLAGS][TILDE]=1;
				*ptrs[TILDE_OFFSETS]=i;
				ptrs[TILDE_OFFSETS]++;
				*ptrs[NUM_TILDES]=*ptrs[NUM_TILDES]+1;				
				}
			i++;
			numwords++;
			}	
		}
	/*
	*numwords is different from size.
	*numwords is the total number of
	*char ptrs used in argv. In other 
	*words, the number of commands + arguments
	*Size is the last return value of getword(w)
	*before exiting the loop. (Having failed the condition)
	*If have collected no commands or arguments, and have
	*encountered EOF then return -255.
	*/
	
	if(numwords==0 && size== -255)
		return size;
	
	//If exit loop while & was last word, then
	//its indeed a background process flag
	if(last_word_amper)
		ptrs[FLAGS][BACKGR]=1;
	//if have collected no words other than <,>,|... set erorr flag (mimicing real shell)
	if(numwords==0 && (ptrs[FLAGS][INPUT_RED] || ptrs[FLAGS][OUTPUT_RED] || ptrs[FLAGS][BACKGR] || ptrs[FLAGS][HERE_IS]) ){
		ptrs[FLAGS][ERROR]=1;
		ptrs[ERRORS][3]=1;
		}
	
	if (*ptrs[NUM_PIPES]>10){
		ptrs[FLAGS][ERROR]=1;
		ptrs[ERRORS][2]=1;
		}
	if(ptrs[FLAGS][HERE_IS] && ptrs[FLAGS][INPUT_RED]){
		ptrs[FLAGS][ERROR]=1;
		ptrs[ERRORS][1]=1;
		}
	ptrs[PIPE_OFFSETS]=save_pipe_offset;	
	ptrs[ENV_OFFSETS]=save_env_offset;
	ptrs[TILDE_OFFSETS]=save_tilde_offset;
	argv[i]=NULL;
	return numwords;			

}
